
#include <Wire.h>
#include "SparkFunBME280.h"
#include <SparkFunCCS811.h>
#include <SD.h>
#include <SPI.h>
#include <ArduinoJson.h>
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>

#define TX 4
#define RX 3




//#define PIN_NOT_WAKE 5
#define CCS811_ADDR 0x5B

void ReadSaveSensorData();
File myFile;


CCS811 myCCS811(CCS811_ADDR);
BME280 mySensor;
bool aux, aux2, auxsd = false;

void setup()
{
  Serial.begin(9600);
  Serial.println("Comunicacion serial iniciada");
 
  if (!SD.begin(8)) {
    Serial.println("No se pudo inicializar");
    return;
  }
  else {
    Serial.println("inicializacion exitosa de la SD");
  }

  Wire.begin();
  Wire.setClock(400000); //Increase to fast I2C speed!

  CCS811Core::status returnCode = myCCS811.begin();
  if (returnCode == CCS811Core::SENSOR_SUCCESS){
    aux2 = true;
  }
  if (mySensor.beginI2C()){
    aux = true;
  }
  mySensor.setMode(MODE_SLEEP); //Sleep for now
}


void loop(){

  StaticJsonDocument<200> doc;

  Serial.println("Capturando data");
  Serial.flush();

  if (aux2){
    if (myCCS811.dataAvailable()){
    //Calling this function updates the global tVOC and eCO2 variables
    myCCS811.readAlgorithmResults();

    if (aux){
      mySensor.setMode(MODE_FORCED); //Wake up sensor and take reading

      //long startTime = millis();
      while(mySensor.isMeasuring() == false) ; //Wait for sensor to start measurment
      while(mySensor.isMeasuring() == true) ; //Hang out while sensor completes the reading    
      //long endTime = millis();

      float BMEhumid = mySensor.readFloatHumidity();
      float BMEtempC = mySensor.readTempC();
      Serial.print("Hum: ");
      Serial.print(BMEhumid);
      Serial.print(" - Temp: ");
      Serial.println(BMEtempC);
      Serial.flush();

      //This sends the temperature data to the CCS811
      myCCS811.setEnvironmentalData(BMEhumid, BMEtempC);
    }

      doc["co2"] = myCCS811.getCO2();
      doc["tvoc"] = myCCS811.getTVOC();
      
      //serializeJsonPretty(doc, myFile);
      // if (serializeJsonPretty(doc, myFile) == 0) {
      // Serial.println(F("Failed to write to file"));
      // }

      

      //ReadSaveSensorData();
  }
  }

  
  	  myFile = SD.open("PRUEBA12.txt", FILE_WRITE);//abrimos  el archivo
      if (!myFile) {
        Serial.println(F("Failed to create file"));
        return;
      }
      serializeJson(doc, Serial);
      serializeJson(doc, myFile);
      myFile.close();
      Serial.println();
      delay(5000);
}

void ReadSaveSensorData(){

}